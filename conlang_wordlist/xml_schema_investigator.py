"""
Utility to generate a general JSON schema from an xml file.

"""

__author__ = 'eric'


import json
import sys
try:
    # noinspection PyPep8Naming
    import xml.etree.cElementTree as ET
except ImportError:
    # noinspection PyPep8Naming
    import xml.etree.ElementTree as ET


# Helpers
# ==========================

class SetEncoder(json.JSONEncoder):
    """
    JSON encoder that returns sets as lists/arrays.
    Courtsey of SO: http://goo.gl/SGii4e

    Usage: `json.dumps(set([1, 2, 3, 4, 5]), cls=SetEncoder)
    => '[1, 2, 3, 4, 5]'`

    """
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


# Main Functions
# ==========================

def main(tree):
    """
    Runs the xml schema inspector using depth first traversal
    and accumulating a schema in JSON form.  Returns the schema.

    :param tree: An xml element tree
    :type tree: xml.etree.ElementTree.ElementTree
    :return: A dictionary that can be dumped into JSON using SetEncoder class
    """
    root = tree.getroot()
    return runner(root)


def runner(elem):
    schema = {}

    # Depth First Iteration over all elements
    for child in elem:
        helper(child, schema)

    return schema


def helper(elem, schema):
    # Get the tag and attributes
    tag = elem.tag
    # Check schema for the tag and add it if it's not there
    if tag in schema:
        # Add any new attributes
        add_attributes(schema[tag], elem)
    # If it's not there, add it
    else:
        # Recursive Case: Add attributes and children
        schema[tag] = {
            'attributes': {name for name in elem.attrib.keys()},    # set
            'children': runner(elem)                                # dict
        }


def add_attributes(schema_elt, new_elt):
    """
    Adds attributes and types to a tag in the schema.

    :param schema_elt: The element as is currently appears in the schema
    :param new_elt: A new instance of the element type
    :return: None
    """
    # TODO: Add attribute types?
    curr_attrs = schema_elt['attributes']
    new_attrs = {name for name in new_elt.attrib.keys()}
    # Update old with new
    curr_attrs.update(new_attrs)


if __name__ == '__main__':
    # TODO: Ensure file exists

    # Get the tree
    xml_tree = ET.ElementTree(file=sys.argv[1])

    # Get the schema
    parsed_schema = main(xml_tree)

    # Print it
    print json.dumps(parsed_schema, indent=2, sort_keys=True, cls=SetEncoder)
