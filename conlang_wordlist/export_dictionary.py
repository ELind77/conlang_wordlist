"""
Export Dictionary

Script to extract a nicely formatted dictionary from a conlang *.cdic xml
file.

"""

from __future__ import unicode_literals
import re
import codecs
try:
    # noinspection PyPep8Naming
    import xml.etree.cElementTree as ET
except ImportError:
    # noinspection PyPep8Naming
    import xml.etree.ElementTree as ET
from nltk.corpus import words

__author__ = 'Eric Lind'


# Globals
# ============================
RE_NEWLINES = re.compile(r"((?:[\n\r]){2,})")
WORD_LIST = set(words.words())



# Main Functions
# ============================
def main(conlang_dict_path, out_file=None):
    assert out_file is not None, "No output file given!"
    full_dictionary = load_dict(conlang_dict_path)
    
    with codecs.open(out_file, mode='w', encoding='utf-8') as _f:
        for wd in full_dictionary.iter(tag='wordDef'):
            # print format_word(wd), "\n\n".encode('utf-8')
            formatted = format_word(wd)
            if formatted:
                # print type(formatted)
                # print "Word:".encode('utf-8'), formatted
                # print "\n"
                _f.write(formatted.decode('utf-8'))
            _f.write('\r\n\r\n')
        

def load_dict(conlang_dict_path):
    return ET.ElementTree(file=conlang_dict_path)


def format_word(word_def):
    """

    :param word_def: XML wordDef element
    :return: str
    """
    try:
        lexeme = get_lexeme(word_def).encode('utf-8')
        translation = get_definition(word_def).encode('utf-8')
        return lexeme + "\n".encode('utf-8') + translation
    except AttributeError:
        return None


def get_lexeme(word_def):
    """Extracts the word from the xml"""
    # This is kind of a pain since the schema may be irregular
    # Sometimes things are empty...
    attribs = word_def.attrib

    if 'name' in attribs:
        name = attribs['name']
        # Make sure there's something here
        if name:
            return name
        else:
            return "EMPTY NAME??!?".encode('utf-8')
    else:
        return 'NO NAME??!?'.encode('utf-8')


def get_definition(word_def):
    """Extracts the definition from a word"""
    # Some words have an empty definition..which is just weird
    defn = word_def.find('definition')

    if defn is not None:
        # Make sure there's text
        defn_text = defn.text
        if defn_text:
            sane_text = re.sub(RE_NEWLINES, '\n', defn_text)
            # Check each line, looking for conjugations
            result = ''
            sane_lines = sane_text.split('\n')
            for i in range(len(sane_lines)):
                if is_conjugation(sane_lines[i]):
                    result += "Conjugations:\n"
                    result += '\n'.join(sane_lines[i:])
                    break
                else:
                    result += "%s\r\n\r\n" % sane_lines[i].strip()
            return result
        else:
            # TODO: Count occurances our output errors
            return "EMPTY DEFINITION??!?".encode('utf-8')
    else:
        return "NO DEFINITION??!?".encode('utf-8')


def is_conjugation(line):
    tokens = line.strip().lower().split()
    if len(tokens) > 1:
        return False
    if any(ord(ch)>128 for ch in line):
        return True
    if any(w not in WORD_LIST for w in tokens):
        return True
    return False
    

def replace_with_newline_counts(s):
    """
    :param s:
    :type s: str
    :return:
    """
    s = s.replace('\r', '')
    curr_count = 0
    acc = ''
    
    for ch in s:
        if ch != '\n':
            if curr_count > 0:
                acc += ' %s<NL> ' % curr_count
                curr_count = 0
            acc += ch
        else:
            curr_count += 1
    return acc


# Run it!
if __name__ == '__main__':
    import plac
    plac.call(main)
